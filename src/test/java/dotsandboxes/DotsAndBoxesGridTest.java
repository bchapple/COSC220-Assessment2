package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);
    private DotsAndBoxesGrid testGrid;
    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @BeforeEach
    public void init(){
        //Create a fresh DotsAndBoxesGrid, for each test
         testGrid = new DotsAndBoxesGrid(3, 3, 2);
    }
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.

    @Test
    public void testBox1Sides()
    {
        //Draw one side and test
        testGrid.drawHorizontal(0,0,1);
        logger.info("Tests that the box is incomplete, with only one side.");
        assertFalse(testGrid.boxComplete(0,0));
    }

    @Test
    public void testBox2Sides()
    {
        //Draw two sides and test
        testGrid.drawHorizontal(0,0,1);
        testGrid.drawVertical(0,0,1);
        logger.info("Tests that the box is incomplete, with only two sides.");
        assertFalse(testGrid.boxComplete(0,0));
    }

    @Test
    public void testBox3Sides()
    {
        //Draw three sides and test
        testGrid.drawHorizontal(0,0,1);
        testGrid.drawVertical(0,0,1);
        testGrid.drawVertical(1,0,1);
        logger.info("Tests that the box is incomplete, with only threes sides.");
        assertFalse(testGrid.boxComplete(0,0));
    }
    @Test
    public void testBox4Sides()
    {
        //Draw four sides and test
        testGrid.drawHorizontal(0,0,1);
        testGrid.drawVertical(0,0,1);
        testGrid.drawVertical(1,0,1);
        testGrid.drawHorizontal(0,1,1);
        logger.info("Tests that the box is complete, with only threes sides.");
        assertTrue(testGrid.boxComplete(0,0));
    }



    @Test
    public void testRedrawnLineHorizontal()
    {
        //Draw a horizontal line and test reaction for redrawing line
        testGrid.drawHorizontal(1,1,1);
        logger.info("Tests whether the function throws an illegalStateException\n" +
                "for a redrawn horizontal line");

        assertThrows(IllegalStateException.class,()->testGrid.drawHorizontal(1,1,1));
    }


    @Test
    public void testRedrawnLineVertical()
    {
        //Draw a vertical line and test reaction for redrawing line
        testGrid.drawVertical(1,1,1);
        logger.info("Tests whether the function throws an illegalStateException\n" +
                "for a redrawn vertical line");

        assertThrows(IllegalStateException.class,()-> testGrid.drawVertical(1,1,1));

    }



}
